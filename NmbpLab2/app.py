from flask import Flask, render_template, request
from flask_pymongo import PyMongo
from datetime import datetime
from bson import ObjectId

#api key : 9a69d40333934edc8e62df435a5d7601

app = Flask(__name__)

app.config["MONGO_URI"] = "mongodb://localhost:27017/myDatabase"
mongo = PyMongo(app)



@app.route('/', methods=['GET', 'POST'])
def home():
    posts = mongo.db.myDatabase.find().sort([("_id", -1)]).limit(10)
    if request.method == 'POST':
        #dohvati id
        query = {"_id": {"$eq": ObjectId(request.form['id'])}}
        new_values = {"$push": {"comments": {"dateModified": datetime.today().__str__(), "comment": request.form['comment']}}}
        mongo.db.myDatabase.update(query, new_values)

    return render_template("home.html", posts=posts)


if __name__ == '__main__':
    app.run(debug=True)
