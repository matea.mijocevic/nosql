-Korišten je Python 3.7 i Flask za izradu web aplikacije uz bazu MongoDB
(Detaljniji popis u requirements.txt datoteci)
-Unos podataka: preko stranice news.org i kaggle datasets sam dohvatila vijesti u json formatu
te ih ubacila u bazu pomoću Robo 3T
-Aplikacija se pokreće naredbom python app.py

1.zadatak upis komentara:
query = {"_id": {"$eq": ObjectId(request.form['id'])}}
new_values = {"$push": {"comments": {"dateModified": datetime.today().__str__(), "comment": request.form['comment']}}}
mongo.db.myDatabase.update(query, new_values)


//db.myDatabase.update({'comments': {$exists : false}}, {$set: {'comments': []}}, {'multi':true})

2.zadatak
M/R upiti:

a)***********

var map = function() {
    if(this.comments != null) {
        emit({numOfComm: this.comments.length},
            {numOfNews: 1});
     }
}

var reduce = function(key, values) {
	var rv = {
            numOfNews: 0
            };
        values.forEach(function(value) {
                  rv.numOfNews += value.numOfNews;
        });
        return rv;
}

var fin = function(key, reducedVal) {
	var rv = {
            numOfNews: reducedVal.numOfNews
        };
        return rv;
}

db.myDatabase.mapReduce(map, reduce, {out: 'mr_a', finalize: fin});

b)*****************

var map = function() {
    if(this.comments != null && this.comments.length > 0) {
        emit({},{numOfNews: 1, all: 1});
     }
     emit({},{numOfNews: 0, all: 1});
}

var reduce = function(key, values) {
	var rv = {
            numOfNews: 0,
            all:0
	};
	values.forEach(function(value) {
		rv.numOfNews += value.numOfNews;
		rv.all += value.all;
	});
	return rv;
}

var fin = function(key, reducedVal) {
	var diff = reducedVal.numOfNews / reducedVal.all * 100;
	diff = diff.toFixed(3);
	diff += "%";
	var rv = {
		precentage: diff
	};
	return rv;
}

db.myDatabase.mapReduce(map, reduce, {out: 'mr_b', finalize: fin});

c)**************

var map = function() {
        if(this.content === null) return;
        var words = this.content.split(/[,. ]/);
	if(words){
            for(var i=0; i<words.length; i++){
                    emit(this.author, {word: words[i], num:1});
            }
	}
};

var reduce = function(key, values) {
	var rv = {
		words:[]
	};
      	var dict = [];	//{word, num}
	var words = [];

	values.forEach( function(value) {
		if(words.indexOf(value.word) != -1){
                    for(var i=0; i<dict.length; i++){
                            if(dict[i].word === value.word)
                                    dict[i].num += 1;
                    }
		}else if (value.word !== undefined && value.word !== "" ){
			words.push(value.word);
			dict.push({word: value.word, num:value.num});
		}
	});

	dict = dict.sort(function(a,b){
		return b.num-a.num;
	});
	rv.words = dict
	return rv;
};

var fin = function(key, reducedVal){
        var j = 1;
        var finVal = [];

	for(var i =0; i<reducedVal.words.length; i++){
		if(j <11){
			finVal.push([reducedVal.words[i].word, reducedVal.words[i].num]);
		}else{
			break;
		}
		j +=1;
	};
	return finVal;
}

db.myDatabase.mapReduce(map, reduce, {out: 'mr_c', finalize: fin});